extends RigidBody

# Ao entrar em contato com node do grupo "destrutivel", atinge o node com dano
func _on_BolaCanhao_body_entered(body):
	if body.is_in_group("destrutivel"):
		$AudioExplosao.play()
		$Explosao.emitting = true
		$CollisionShape/MeshInstance.visible = false
		$CollisionShape.disabled = true
		body.recebe_dano(10)

# Contador iniciado automaticamente para deletar o objeto
func _on_Timer_timeout():
	queue_free()