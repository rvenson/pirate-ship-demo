extends "res://Scripts/Barco.gd"

signal morte()

func _process(delta):
	input_handler()

# Recebe os inputs do jogador
func input_handler():
	if Input.is_action_pressed("acelerar"):
		movimento = 1
	if Input.is_action_pressed("freiar"):
		movimento = -1
	if Input.is_action_pressed("virar_direita"):
		rotacao = -1
	if Input.is_action_pressed("virar_esquerda"):
		rotacao = 1
	if Input.is_action_just_pressed("atirar"):
		canhao = true
		if Input.is_action_pressed("auxiliar"):
			modo_camera_na_bala = true
		else:
			modo_camera_na_bala = false

# Ao morrer, emite um sinal de morte para outros nós
func morte():
	.morte() # executa função do pai
	emit_signal("morte")