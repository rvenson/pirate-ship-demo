extends "res://Scripts/Barco.gd"

var navegador : Navigation
var target : Node

# Inicia referências do jogador e da navegação
func _ready():
	navegador = get_node("/root/Jogo/Mundo/Cena/Terreno/Navegador") as Navigation
	target = get_node("/root/Jogo/Mundo/Cena/Objetos/Jogador/Barco") as Node
	
# Enquanto houver um target, calcula o próximo ponto pelo navegador
# verifica o ângulo entre a frente do barco e a posição do jogador
# Aplica a rotação baseado no ângulo
# Por fim, atira se estiver na reta e na distância definida
func _physics_process(delta):
	if target is Node:
		var proximo_ponto = navegador.get_closest_point(target.global_transform.origin)
		var angulo = -global_transform.origin.direction_to(proximo_ponto).dot(global_transform.basis.x)
		rotacao = angulo
		var distancia = global_transform.origin.distance_to(target.global_transform.origin)
	
		if angulo < 0.05 and angulo > -0.05:
			if distancia > 30:
				movimento = 1
			else:
				canhao = true