extends RigidBody

var movimento = 0
var rotacao = 0
var aceleracao = 2000
var max_velocidade = 5
var canhao = false
var canhao_carregado = false
export var vida = 100
var modo_camera_na_bala = false
var morto = false

signal bala_disparada(bala)

# Executa funções básicas e reseta variáveis
func _physics_process(delta):
	movimentacao()
	armas()
	
	rotacao = 0
	movimento = 0
	canhao = false
	
# Realiza movimentação e rotação
func movimentacao():
	angular_velocity.y = rotacao
	# Limita a velocidade e aplica força no rigidbody
	if linear_velocity.length() < max_velocidade:
		add_force(movimento * aceleracao * -global_transform.basis.z, Vector3())
	
# Carrega a bala do canhão e aplica força
func armas():
	if canhao and canhao_carregado and !morto:
		var bala_resource = preload("res://Nodes/BolaCanhao.tscn")
		var bala = bala_resource.instance()
		bala.transform.origin = $"3DPCanhao".global_transform.origin
		bala.add_force((-global_transform.basis.z + Vector3(0, 0.2, 0)) * 1600, Vector3())
		get_node("../../Balas").add_child(bala)
		canhao_carregado = false
		$TimerCanhao.start(2) # Inicia timer de recarregamento do canhão
		$"3DPCanhao/AudioCanhao".play()
		if(modo_camera_na_bala):
			emit_signal("bala_disparada", bala) # Emite sinal caso tenha sido usado bullet time

# Recebe dano e verifica se o barco foi destruído
func recebe_dano(dano):
	vida -= dano
	if vida <= 0:
		morte()

# Executa animação de morte e desabilita colisor
func morte():
	morto = true
	$CollisionShape.disabled = true
	$AnimationPlayer.play("death")

# Recarrega o canhão
func _on_TimerCanhao_timeout():
	canhao_carregado = true
