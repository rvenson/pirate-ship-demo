extends Spatial

# Executa som de "splash" caso algum objeto atinja a agua
func _on_Area_body_entered(body):
	if body.is_in_group("objeto"):
		body.get_node("AudioAgua").play()