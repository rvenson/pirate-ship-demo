extends Camera

export var target : NodePath
export var speed = 5
var look : Node

# Segue o target caso não seja nulo, aproximando-se a cada frame
func _process(delta):
	if(get_node_or_null(target) != null):
		var offset = (get_node(target).global_transform.origin - global_transform.origin) * 0.01 * speed
		global_transform.origin += offset
		global_transform = global_transform.looking_at(look.global_transform.origin, Vector3.UP)